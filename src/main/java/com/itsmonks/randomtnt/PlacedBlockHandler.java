package com.itsmonks.randomtnt;

import java.util.Random;

import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.level.Level;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraft.world.entity.item.PrimedTnt;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class PlacedBlockHandler {
    private final Random rnd;

    public PlacedBlockHandler(){
        rnd = new Random();
    }

    @SubscribeEvent
    public void placedBlock(BlockEvent.EntityPlaceEvent event){
        boolean place_tnt = shouldPlaceTNT();
        if(!place_tnt) return;

        BlockPos blockPos = event.getPos();

        if(!event.getWorld().isClientSide()) {
            PrimedTnt tnt = new PrimedTnt(EntityType.TNT, (Level)event.getWorld());
            tnt.moveTo(blockPos.getX(), blockPos.getY(), blockPos.getZ());
            event.getWorld().addFreshEntity(tnt);
        }
    }

    private boolean shouldPlaceTNT(){
        int randomNumber = this.rnd.nextInt(10) + 1;
        return randomNumber > 7;
    }
}
